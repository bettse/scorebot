const {InfluxDB, Point} = require('@influxdata/influxdb-client');
const Bot = require('keybase-bot');

const {
  LAMBDA_TASK_ROOT,
  PATH,
  KEYBASE_BOT_USERNAME,
  KEYBASE_BOT_PAPERKEY,
} = process.env;

const success = {
  statusCode: 200,
  body: '',
};

const gopath = `${LAMBDA_TASK_ROOT}/gopath/bin`;
process.env['PATH'] = `${PATH}:${gopath}`;

exports.handler = async function(event) {
  try {
    const {body: json} = event;
    const body = JSON.parse(json);
    console.log(body);

    const bot = new Bot();
    const username = KEYBASE_BOT_USERNAME;
    const paperkey = KEYBASE_BOT_PAPERKEY;
    await bot.init(username, paperkey, {verbose: true});
    const message = {
      body: `Hello kbot! This is ${
        bot.myInfo().username
      } saying hello from my device ${bot.myInfo().devicename}`,
    };
    console.log('Message', message);
  } catch (e) {
    console.error(error);
  } finally {
    await bot.deinit();
  }
  return success;
};
